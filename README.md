# Flux

## Installation
Install all themes with one command

```bash
make install
```

## Install KDE Theme
Installation of KDE Theme is easy

```bash

cd flux
chmod +x install.sh
./install.sh

```

## Installing GTK theme

```bash

cd flux-gtk
chmod +x install.sh
./install.sh

```
## About
Flux theme is a combination of Pear Dark and Vimix GTK

```
(c) Srevin Saju
```
